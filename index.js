console.log ("Hello World!")


let firstName = "Darla Mae";
console.log("First name: " + firstName);

let lastName = "Cañeca";
console.log("Last name: " + lastName);

let age = 30;
console.log("Age: " + age);

let myHobby = ("Hobbies: ")
console.log(myHobby)

let hobbies = ["Music", "Anime", "Crochet"];
console.log(hobbies)


let address = ("Work address: ")

let workAddress = {
	houseNumber: 123,
	street: "Liko liko street",
	city: "SMB",
	state: "Bulacan",
}
console.log(address)
console.log(workAddress);

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);
	
let friends = ["Tony", "Bruce", "Thor", "Natasha" ,"Clint", "Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,
}
console.log("My Full Profile: ")
console.log(profile);

let bestFriend = "Bucky Barnes";
console.log("My bestfriend is: " + bestFriend);

const lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);